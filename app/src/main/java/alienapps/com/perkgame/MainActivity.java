package alienapps.com.perkgame;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.perk.perksdk.PerkInterface;
import com.perk.perksdk.PerkListener;
import com.perk.perksdk.PerkManager;

public class MainActivity extends Activity {
    String key = "ff988bf37f70796b256abf405ea35d2c9dfbc43f";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*final PerkListener localPerkListener = new PerkListener() {
            @Override
            public void onPerkEvent(String reason) {
                Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_SHORT).show();
            }
        };*/
        PerkManager.startSession(MainActivity.this, key);
    }

    public void trackEvent(View view) {
        PerkManager.trackEvent(this, key, "5902d296dee5aa8c07e0151e3709f4fc540e363c", true, new PerkInterface() {
            @Override
            public void showEarningDialog() {
                Toast.makeText(getApplicationContext(), "Event has been tracked", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
