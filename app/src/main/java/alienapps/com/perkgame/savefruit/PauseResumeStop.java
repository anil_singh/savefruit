package alienapps.com.perkgame.savefruit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import alienapps.com.perkgame.R;

public class PauseResumeStop extends Activity
{
	ImageView pauseBtn,startBtn,stopBtn;
	Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		Log.d("", "Hi donot divert");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pauseresume);
	}
	
	public void prsMethod(View v)
	{
		intent=getIntent();
		switch(v.getId())
		{
		case R.id.menu:

			Intent intent1 = new Intent(PauseResumeStop.this,StartPage.class);
			intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent1);
			
			break;
		case R.id.resume:
			Log.d("", "Hi donot divert");
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			PauseResumeStop.this.finish();
			break;
			
		case R.id.restart:
			int id = intent.getIntExtra("id", 0) ;
			Log.d("", "Hi donot divert "+id);
			if(id == 1)
			{
				Intent intent2 = new Intent(PauseResumeStop.this,FruitBucketEasyMode.class);
				intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent2);
			}
			else if(id == 2)
			{
				Intent intent3 = new Intent(PauseResumeStop.this,FruitBucketMediumMode.class);
				intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent3);
			}

			else if(id == 3)
			{
				Intent intent4 = new Intent(PauseResumeStop.this,FruitBucketHardMode.class);
				intent4.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent4);
			}
			break;

		}
	}
	
	@Override
	protected void onRestoreInstanceState (Bundle restoreBundle)
	{
		super.onRestoreInstanceState(restoreBundle);
	}
	
	@Override
	protected void onPause() 
	{
		super.onPause();
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
	}
}