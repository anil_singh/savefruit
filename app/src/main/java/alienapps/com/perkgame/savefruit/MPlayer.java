package alienapps.com.perkgame.savefruit;

import java.io.IOException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Vibrator;
import android.preference.PreferenceManager;

import alienapps.com.perkgame.R;

public class MPlayer
{
	public  static AudioManager audioManager;
	public  static MediaPlayer mp;
	public  static SoundPool soundPool;
	
	public static int coin_sound;
	public static int drop_sound;
	public static int bg_sound;
	public static int blast_sound;
	public static int high_sound;
	public static int perfect_sound;
	public static int jump_sound;
	public static int crash_sound;
	public static int mgt_sound;
	public static int btn_sound;
	
	public static Vibrator vib;
	
	static int EFFECT_ZOMBIEGOOD=0;
	static int EFFECT_ZOMBIE_BAD=1;
	static float effectVolumn;
	static  float backgroundVolumn=10f;
	static Context mcontext;
	static SharedPreferences  preferences;
	static boolean is_music ;
	public static void initBackgroundMusic(Context context)
	{
		mcontext = context;
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
		is_music = preferences.getBoolean("IsMusic", true);
		mp=new MediaPlayer();
		try 
		{
			AssetFileDescriptor asset=context.getAssets().openFd("blind.mp3");
			mp.setDataSource(asset.getFileDescriptor(),asset.getStartOffset(),asset.getLength());
			mp.prepare();
			mp.setLooping(true);
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public static void playBackgroundMusic()
	{
		if(mp==null)
		{
			initBackgroundMusic(mcontext);
			mp.start();
		}
//		else if(is_music)
//			mp.start();
	}
	
	public static void pauseBackgroundMusic()
	{
		if(mp!=null && mp.isPlaying())
        {
			mp.pause();
        }
	}
	
	public static void resumeBackgroundMusic()
	{
		if(!mp.isPlaying())
		{
			initBackgroundMusic(mcontext);
			mp.start();
		}
		else if(!mp.isPlaying())
		{
			initBackgroundMusic(mcontext);
			mp.start();
		}
	}
	
	public  static void stopBackgroundMusic() 
	{
		// TODO Auto-generated method stub
		mp.stop();
		mp.release();
		mp=null;
	}
	
	public static  boolean  turnOffBackgroundMusic()
	{
        mp.setVolume(0, 0);
		return true;
	}
	
	public static boolean  turnOnBackgroundMusic()
	{
		mp.setVolume(backgroundVolumn, backgroundVolumn);
		return true;
	}
	
	public static void initeffect(Context context)
	{
		soundPool= new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
//		bg_sound = soundPool.load(context, R.raw.blind, 1);
		
		btn_sound = soundPool.load(mcontext, R.raw.btn_clk, 1);
		drop_sound = soundPool.load(mcontext, R.raw.dropinbucket, 1);
	}
	
}