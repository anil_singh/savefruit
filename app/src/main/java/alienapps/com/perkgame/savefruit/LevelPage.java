package alienapps.com.perkgame.savefruit;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import alienapps.com.perkgame.R;

public class LevelPage extends Activity
{
	protected static final int TIMER_RUNTIME = 10000;

    protected boolean mbActive;
    protected ProgressBar mProgressBar;
    MediaPlayer bgMusic;
    Vibrator vib;
    ImageButton ebtn,mbtn,hbtn;
    Thread timerThread;
    SharedPreferences pref;
    
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		
		setContentView(R.layout.levelpage);
		
		ebtn=(ImageButton)findViewById(R.id.easyBtn);
		mbtn=(ImageButton)findViewById(R.id.mediumBtn);
		hbtn=(ImageButton)findViewById(R.id.hardBtn);
		
		vib=(Vibrator)getSystemService(VIBRATOR_SERVICE);
		if(pref.getBoolean("IsMusic", true))
		{
			bgMusic=MediaPlayer.create(LevelPage.this, R.raw.islnd_fvr_bgmusic);
			bgMusic.setLooping(true);
			bgMusic.start();
		}

		
	}
	
	
	
	@Override
	protected void onRestoreInstanceState (Bundle restoreBundle)
	{
		super.onRestoreInstanceState(restoreBundle);
	}
	
	

	
	@Override
	public void onResume()
	{
		super.onResume();
	}
	
	@Override
	protected void onPause()
	{		
		super.onPause();
		if(pref.getBoolean("IsMusic", true))
			bgMusic.release();
	}
	
	
	public void difficultLevelMethod(View v)
	{
		switch(v.getId())
		{
			case R.id.easyBtn:
				
				ebtn.setClickable(false);
				mbtn.setClickable(false);
				hbtn.setClickable(false);
				
				vib.vibrate(30);
				
				mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
				mProgressBar.setVisibility(View.VISIBLE);
				
				  timerThread = new Thread() 
				{
			          @Override
			          public void run()
			          {
			              mbActive = true;
					              try {
						                  int waited = 0;
						                   while(mbActive && (waited < TIMER_RUNTIME)) 
						                   {
						                      sleep(80);
						                      if(mbActive) 
						                        {
					                          waited +=200;
					                          updateProgress(waited);
						                        }
						                   }
					              	  } catch(InterruptedException e) {} 
					              		finally {
					              					startActivity(new Intent(LevelPage.this,FruitBucketEasyMode.class));
					              				}
			        
			          	}

					private void updateProgress(final int timePassed)
					{
						if(null != mProgressBar) {
					           // Ignore rounding error here
					           final int progress = mProgressBar.getMax() * timePassed / TIMER_RUNTIME;
					           mProgressBar.setProgress(progress);
					       }
						
					}
				};
			     timerThread.start();			
			   //  bgMusic.start();
				
				break;
				
				
			case R.id.mediumBtn:
				
				ebtn.setClickable(false);
				mbtn.setClickable(false);
				hbtn.setClickable(false);
				
				vib.vibrate(30);
				
				mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
				mProgressBar.setVisibility(View.VISIBLE);
				
			    timerThread = new Thread() 
				{
			          @Override
			          public void run()
			          {
			              mbActive = true;
					              try {
						                  int waited = 0;
						                   while(mbActive && (waited < TIMER_RUNTIME)) 
						                   {
						                      sleep(80);
						                      if(mbActive) 
						                        {
					                          waited +=200;
					                          updateProgress(waited);
						                        }
						                   }
					              	  } catch(InterruptedException e) {} 
					              		finally {
					              					startActivity(new Intent(LevelPage.this,FruitBucketMediumMode.class));
					              				}
			        
			          	}

					private void updateProgress(final int timePassed)
					{
						if(null != mProgressBar) {
					           // Ignore rounding error here
					           final int progress = mProgressBar.getMax() * timePassed / TIMER_RUNTIME;
					           mProgressBar.setProgress(progress);
					       }
						
					}
				};
			     timerThread.start();			
			  //   bgMusic.start();
				
				break;
				
			case R.id.hardBtn:
				
				ebtn.setClickable(false);
				mbtn.setClickable(false);
				hbtn.setClickable(false);
				
				vib.vibrate(30);
				
				mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
				mProgressBar.setVisibility(View.VISIBLE);
				
				timerThread = new Thread() 
				{
			          @Override
			          public void run()
			          {
			              mbActive = true;
					              try {
						                  int waited = 0;
						                   while(mbActive && (waited < TIMER_RUNTIME)) 
						                   {
						                      sleep(80);
						                      if(mbActive) 
						                        {
					                          waited +=200;
					                          updateProgress(waited);
						                        }
						                   }
					              	  } catch(InterruptedException e) {} 
					              		finally {
					              					startActivity(new Intent(LevelPage.this,FruitBucketHardMode.class));
					              				}
			        
			          	}

					private void updateProgress(final int timePassed)
					{
						if(null != mProgressBar) {
					           // Ignore rounding error here
					           final int progress = mProgressBar.getMax() * timePassed / TIMER_RUNTIME;
					           mProgressBar.setProgress(progress);
					       }
						
					}
				};
			     timerThread.start();			
			   //  bgMusic.start();
				
				break;
		}
		
	}
	@Override
	public void onBackPressed()
	{
		// TODO Auto-generated method stub
		if (timerThread!=null)
		{
			android.os.Process.killProcess(android.os.Process.myPid());
		}
		Intent intent = new Intent(this, StartPage.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		super.onBackPressed();
	}
}
