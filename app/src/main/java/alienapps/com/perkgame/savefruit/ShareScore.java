package alienapps.com.perkgame.savefruit;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import alienapps.com.perkgame.R;

@SuppressWarnings("unused")
public class ShareScore
{
	Activity activity;
	String score;
	String msg;
	byte[] dat;
	@SuppressWarnings("deprecation")
	public ShareScore(Activity mactivity, String myscore, String mode, byte[] data) 
	{
        activity = mactivity;
        score = myscore;
        msg = "I am playing SaveFruit Game. And I got massive score "+myscore+" in "+mode+".\n Can you beat my score";
        dat = data;
	}
	

	
	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) 
	{
	    for (String string : subset) 
	    {
	        if (!superset.contains(string)) 
	        {
	            return false;
	        }
	    }
	    return true;
	}
	
	public void onTost(String msg) 
	{
		// TODO Auto-generated method stub
//		Log.d("", "Posted Successfully_1");
        LayoutInflater lyIn = activity.getLayoutInflater();
        View layout = lyIn.inflate(R.layout.custum_tost, (ViewGroup)activity.findViewById(R.id.toast_custom));
        TextView txtvw = (TextView) layout.findViewById(R.id.tvtoast);
        txtvw.setText(msg);
        txtvw.setTextColor(Color.WHITE);
        txtvw.setGravity(Gravity.CENTER);
        Toast tost = new Toast(activity.getApplicationContext());
        tost.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        tost.setDuration(Toast.LENGTH_SHORT);
        tost.setView(layout);
        tost.show();
	}
}
//49GSY2liIwcNIm+mgTC3DZbo72c=
//App ID:	696458423718208





/*
  try {
            PackageInfo info = getPackageManager().getPackageInfo("com.ids.fbpost", 
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:","Key-->"+ Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
        } catch (NameNotFoundException e) 
        {
        	Log.d("", "Key Find");
        } catch (NoSuchAlgorithmException e) 
        {
        	Log.d("", "Key Find_1");

        }*/