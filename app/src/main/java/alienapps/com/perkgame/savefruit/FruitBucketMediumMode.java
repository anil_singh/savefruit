package alienapps.com.perkgame.savefruit;

import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;

import alienapps.com.perkgame.R;

public class FruitBucketMediumMode extends Activity implements OnTouchListener
 {
	GraphicBucketClass graphicBC;
	
	Bitmap bmpForest,bmpBucket,bmpStone,bmpSCard,bmpLife,bmpLifeUp, bmpFullBucket, bmpEmptyBuket;
	Bitmap bmpZero,bmpOne,bmpTwo,bmpThree,bmpFour;
	Bitmap bmpApple,bmpCoconut,bmpOrange,bmpMango,bmpBanana,bmpMelon;
	Bitmap bmpLifeDown,bmpSpeedUp,bmpScoreMinus;
	
	int width,height,counter,f,score;
	float x,y;	
	public static int scoreCounter=0;
	boolean paused = true;
	String strScore=null;
	Display display;
	MediaPlayer dropInBucketMusic,gameBgMusic;
	boolean test=false;
	boolean test1=false;
	SharedPreferences pref;
	boolean ismusic;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		/*requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
							 WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		Log.d("", " SCORE ki detail in Medi Mode  " +score );
		*/
		counter=f=score=scoreCounter=0;
		
		display=((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		width=display.getWidth();
		height=display.getHeight();
		
		graphicBC=new GraphicBucketClass(this);
		graphicBC.setOnTouchListener(this);	
	
		registerID(); // Registering all ID of XML Layout 
		
		x=y=0;
		pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		ismusic = pref.getBoolean("IsMusic", true);
		if (ismusic) 
		{
			dropInBucketMusic = MediaPlayer.create(FruitBucketMediumMode.this, R.raw.dropinbuckmusic);
			gameBgMusic = MediaPlayer.create(FruitBucketMediumMode.this, R.raw.mainbgmusic);
			gameBgMusic.start();
			gameBgMusic.setLooping(true);
		}
		
		setContentView(graphicBC);
		
	}
	
	public void registerID()
	{
		bmpForest=BitmapFactory.decodeResource(getResources(), R.mipmap.bgimg2);
		bmpStone=BitmapFactory.decodeResource(getResources(),R.mipmap.stonewall);
		bmpSCard=BitmapFactory.decodeResource(getResources(),R.mipmap.scard2);
		bmpLife=BitmapFactory.decodeResource(getResources(),R.mipmap.lifee);
		bmpEmptyBuket=BitmapFactory.decodeResource(getResources(), R.mipmap.bucket3);
		bmpFullBucket=BitmapFactory.decodeResource(getResources(), R.mipmap.fullbucket);
		bmpForest=Bitmap.createScaledBitmap(bmpForest, width, height, false);
		
		
		// Bitmap Images of Fresh Fruits
		
		bmpApple=BitmapFactory.decodeResource(getResources(), R.mipmap.apple);
		bmpOrange=BitmapFactory.decodeResource(getResources(), R.mipmap.orange);
		bmpCoconut=BitmapFactory.decodeResource(getResources(), R.mipmap.coco);
		bmpMango=BitmapFactory.decodeResource(getResources(), R.mipmap.mango);
		bmpBanana=BitmapFactory.decodeResource(getResources(), R.mipmap.banana);
		bmpMelon=BitmapFactory.decodeResource(getResources(), R.mipmap.mealon);
		
		//Bitmap Images for Numbers
		
		bmpZero=BitmapFactory.decodeResource(getResources(), R.mipmap.zeroo);
		bmpOne=BitmapFactory.decodeResource(getResources(), R.mipmap.onee);
		bmpTwo=BitmapFactory.decodeResource(getResources(), R.mipmap.twoo);
		bmpThree=BitmapFactory.decodeResource(getResources(), R.mipmap.threee);
		bmpFour=BitmapFactory.decodeResource(getResources(), R.mipmap.fourr);
		
		//Introducing Music in Game
//		dropInBucketMusic=MediaPlayer.create(FruitBucketMediumMode.this, R.raw.dropinbucket);
//		gameBgMusic=MediaPlayer.create(FruitBucketMediumMode.this,R.raw.islnd_fvr_bgmusic);
		
		//Power Boost
		
		bmpScoreMinus =BitmapFactory.decodeResource(getResources(), R.mipmap.scoreminus);
		bmpSpeedUp =BitmapFactory.decodeResource(getResources(), R.mipmap.speedup);
		bmpLifeDown =BitmapFactory.decodeResource(getResources(), R.mipmap.bomb);
		bmpLifeUp = BitmapFactory.decodeResource(getResources(), R.mipmap.lifeu2p);
		
	}
	@Override
	protected void onPause() 
	{
		super.onPause();
		graphicBC.pause();	
		if(graphicBC.lifeCounter!=0 && paused)
		{
			Intent intent = new Intent(FruitBucketMediumMode.this,PauseResumeStop.class);
			intent.putExtra("id", 2);
			startActivity(intent);
			paused = false;
		}
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		if (ismusic) 
		{
			dropInBucketMusic.release();
			gameBgMusic.release();
			dropInBucketMusic = MediaPlayer.create(FruitBucketMediumMode.this, R.raw.dropinbuckmusic);
			gameBgMusic = MediaPlayer.create(FruitBucketMediumMode.this, R.raw.mainbgmusic);
			dropInBucketMusic.start();
			gameBgMusic.start();
			gameBgMusic.setLooping(true);
		}
		graphicBC.resume();
		paused = true;
	}
	
	public boolean onTouch(View v, MotionEvent event)
	{
		x=event.getX();
		y=event.getY();
		return true;
	}
	
	public void onBackPressed() 
	{
		if(paused)
		{
			paused = false;
			Intent intent = new Intent(FruitBucketMediumMode.this,PauseResumeStop.class);
			intent.putExtra("id", 2);
			startActivity(intent);
		}
	}		
		
	// Creating the new Graphic bucket Class
	
	class GraphicBucketClass extends SurfaceView implements Runnable
	{
		SurfaceHolder holder;
		Thread mythread=null;
		boolean isItOk=false;
		float anim=0;
			
			int dropPosition1,dropPosition2,fruitCase1,fruitCase2,dropPosition5,lifeDroper;
			int c=0;
		    int lifeCounter=3,fruitCounter=0;
		    
		    int yFromTop= height/100;
		    int yFromTop2 =height/130;
		    int ypowerTop=0;
		    float ySpeed = height/140;
		    float ySpeed2 = height/110;
		    float ySpeed3 = height/120;
		    
			boolean successDrop1=true,successDrop2=true,successDrop3=true,successDrop4=true;
			boolean fresh=true;
			boolean unfresh=false;
			boolean lifeEndChecker=false;		
			protected final Paint paintmyscore =new Paint();	
			private boolean powerDrop=true;
			private int powerCase;
			Bitmap bmp;
		
		public GraphicBucketClass(Context context)
		{
			super(context);
			holder=getHolder();
		}
		
		public void pause()
		{
		   isItOk=false;
		   if(ismusic)
//				dropInBucketMusic.start();
		   gameBgMusic.stop();
			while(true)
			{
				try
				{
					mythread.join();
					mythread.interrupt();	
					
				}catch(Exception e){}
				
			break;
			}
		}
		
		public void resume()
		{
			isItOk=true;
			mythread=new Thread(this);
			mythread.start();
			
		}
		
		// Introducing Score Counting Method		
		public String scoreUpdate()
		{
			strScore=" "+score;
			return strScore;	
			
		}
	
		//Controlling and Checking the fruits to get out of the window frame 
		public void checking() 
		{
			//Checking for Fruit First
			if(dropPosition1<20)
			{
				dropPosition1=dropPosition1+10;
				successDrop1=true;
			}
			else if(dropPosition1>width-bmpMelon.getWidth())
			{
				dropPosition1=dropPosition1-bmpMelon.getWidth();
				successDrop1=true;
			}
			
			//Checking for Fruit Second
			
			if(dropPosition5<20)
			{
				dropPosition5=dropPosition5+10;
				successDrop4=true;
			}
			else if(dropPosition5>width-bmpMelon.getWidth())
			{
				dropPosition5=dropPosition5-bmpMelon.getWidth();
				successDrop4=true;
			}
			
			//Checking for power Booster
			
			if(dropPosition2<20)
			{
				dropPosition2=dropPosition2+10;
				successDrop2=true;
			}
			else if(dropPosition2>width-bmpMelon.getWidth())
			{
				dropPosition2=dropPosition2-bmpMelon.getWidth();
				successDrop2=true;
			}
		}	
				
		public void run()
		{			
			while(isItOk)
			{
				if(!holder.getSurface().isValid())
					continue;
				
				if(score%150==0 && score!=0)
				{
					bmpBucket = bmpFullBucket;	
				}
				else
				{
					bmpBucket = bmpEmptyBuket;
				}
				Canvas canvas=holder.lockCanvas();
						
					/*canvas.drawBitmap(bmpForest, 0, 0, null);
					canvas.drawBitmap(bmpStone,0,height-bmpStone.getHeight(),null);
					canvas.drawBitmap(bmpSCard, 0, height-bmpSCard.getHeight()-5,null);
					canvas.drawBitmap(bmpLife, width-3*(bmpLife.getWidth()/2),height-bmpLife.getHeight()-8,null);*/
				
				canvas.drawBitmap(bmpForest, 0, 0, null);
				canvas.drawBitmap(bmpStone,0,0,null);
				canvas.drawBitmap(bmpSCard, 0, bmpStone.getHeight()/6,null);
				canvas.drawBitmap(bmpLife, width-3*(bmpLife.getWidth()/2),bmpStone.getHeight()/4,null);
				
				//!!!  Painting the score on Canvas !!!//
				
					paintmyscore.setColor(Color.GREEN); 
					paintmyscore.setStyle(Style.FILL); 				 
					paintmyscore.setTextSize(height/20); 
				
				//Declaring random variables// 
		
					Random randomPos1=new Random();
				
				if(successDrop1)
					{
						dropPosition1=randomPos1.nextInt(width-bmpMelon.getWidth());	// Drop Position of Fruit First
						successDrop1=false;
					}
				
				if(successDrop2)
					{
						dropPosition2=randomPos1.nextInt(width-bmpMelon.getWidth());
						successDrop2=false;
					}
				if(successDrop3)
					{
						fruitCase1=randomPos1.nextInt(6); 	// Drop Position of Fruits.......First
						successDrop3=false;
					}
				if(successDrop4)
				{
					fruitCase2=randomPos1.nextInt(6);		// Drop position switching of Fruit Second
					dropPosition5=randomPos1.nextInt(width);	// Drop Position of Width of Second Fruit
				}
				switch(fruitCase1)
							{
								case 0:
									checking();
										canvas.drawBitmap(bmpApple, dropPosition1, yFromTop, null);
											break;
								case 1:
									checking();
										canvas.drawBitmap(bmpCoconut, dropPosition1, yFromTop, null);
											break;
								case 2:
									checking();
										canvas.drawBitmap(bmpOrange, dropPosition1, yFromTop, null);
											break;
								case 3:
									checking();
										canvas.drawBitmap(bmpMango, dropPosition1, yFromTop, null);	
											break;
								case 4:
									checking();
										canvas.drawBitmap(bmpBanana, dropPosition1, yFromTop, null);
											break;
								case 5:
									checking();
										canvas.drawBitmap(bmpMelon, dropPosition1, yFromTop, null);	
											break;									
									
							}
				
//				if(yFromTop < height-bmpBucket.getHeight()-5)
				if(yFromTop < height-bmpBucket.getHeight()/2)
				{
					yFromTop+=ySpeed;
					successDrop1=false;	
				}
				else
				{
					/*lifeCounter--;									
					yFromTop=0;
					successDrop3=true;*/
					
					lifeCounter--;									
					yFromTop=width/5;
					successDrop1=true;
					successDrop3=true;	
					
				}
		
		
	  // Finding out the collision between Bucket and fruits
							
				if( ((x-(bmpBucket.getWidth()/2)<dropPosition1+(bmpMango.getWidth()/2)) &&
						((yFromTop+(bmpMango.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
						((x+bmpBucket.getWidth()/2)>dropPosition1+(bmpMango.getWidth()/2))&&
						(yFromTop+(bmpMango.getHeight()/3))<=height-10)  ||
								      
						((x-(bmpBucket.getWidth()/2)<dropPosition1+(bmpApple.getWidth()/2)) &&
								((yFromTop+(bmpApple.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
								((x+bmpBucket.getWidth()/2)>dropPosition1+(bmpApple.getWidth()/2))&&
								(yFromTop+(bmpApple.getHeight()/3))<=height-10) ||
									   
						((x-(bmpBucket.getWidth()/2)<dropPosition1+(bmpCoconut.getWidth()/2)) &&
							((yFromTop+(bmpCoconut.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
							 ((x+bmpBucket.getWidth()/2)>dropPosition1+(bmpCoconut.getWidth()/2))&&
							  (yFromTop+(bmpCoconut.getHeight()/3))<=height-10) ||
									   
						((x-(bmpBucket.getWidth()/2)<dropPosition1+(bmpOrange.getWidth()/2)) &&
						 ((yFromTop+(bmpOrange.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
						  ((x+bmpBucket.getWidth()/2)>dropPosition1+(bmpOrange.getWidth()/2))&&
							(yFromTop+(bmpOrange.getHeight()/3))<=height-10) ||
									    
						((x-(bmpBucket.getWidth()/2)<dropPosition1+(bmpBanana.getWidth()/2)) &&
						 ((yFromTop+(bmpBanana.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
						  ((x+bmpBucket.getWidth()/2)>dropPosition1+(bmpBanana.getWidth()/2))&&
							(yFromTop+(bmpBanana.getHeight()/3))<=height-10) ||
									     
						((x-(bmpBucket.getWidth()/2)<dropPosition1+(bmpMelon.getWidth()/2)) &&
						  ((yFromTop+(bmpMelon.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
						   ((x+bmpBucket.getWidth()/2)>dropPosition1+(bmpMelon.getWidth()/2))&&
							(yFromTop+(bmpMelon.getHeight()/3))<=height-10)
								      					  
						)
									
		{
			/*dropInBucketMusic.start();
//			successDrop1=true;
			successDrop3=true;									 	
//			counter+=1;
			score+=15;
			yFromTop=0;
			f+=1;
			test=true;*/
					
					
					
//					successDrop1=true;
					successDrop3=true;									 	
//					counter+=10;
					score+=15;
					yFromTop=width/5;
					test=true;
					test1=true;
					f+=1;
					if(ismusic)
						dropInBucketMusic.start();
								
		}			
		
		//************************************************************************************//
		// 		Drawing second Random Fruits with different speed and positions				  //
		//************************************************************************************//
		
		switch(fruitCase2)
		{
			case 0:
				checking();
					canvas.drawBitmap(bmpApple, dropPosition5, yFromTop2, null);
						break;
			case 1:
				checking();
					canvas.drawBitmap(bmpCoconut, dropPosition5, yFromTop2, null);
						break;
			case 2:
				checking();
					canvas.drawBitmap(bmpOrange, dropPosition5, yFromTop2, null);
						break;
			case 3:
				checking();
					canvas.drawBitmap(bmpMango, dropPosition5, yFromTop2, null);	
						break;
			case 4:
				checking();
					canvas.drawBitmap(bmpBanana, dropPosition5, yFromTop2, null);
						break;
			case 5:
				checking();
					canvas.drawBitmap(bmpMelon, dropPosition5, yFromTop2, null);	
						break;									
				
		}

		if(yFromTop2 < height-bmpBucket.getHeight()/2)
			{
				yFromTop2+=ySpeed2;
				successDrop4=false;	
    		}
		else
			{
			  /*lifeCounter--;									
			  yFromTop2=height/100;
//			  ypowerTop=0;
//			  lifeEndChecker=false;
			  successDrop4=true;*/
			
			lifeCounter--;									
//			yFromTop2=width/5;
			 yFromTop2=height/100;
			successDrop4=true;	
			
			}
		
	if(  ((x-(bmpBucket.getWidth()/2)<dropPosition5+(bmpMango.getWidth()/2)) &&
			((yFromTop2+(bmpMango.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
			((x+bmpBucket.getWidth()/2)>dropPosition5+(bmpMango.getWidth()/2))&&
			(yFromTop2+(bmpMango.getHeight()/3))<=height-10)  ||
					      
			((x-(bmpBucket.getWidth()/2)<dropPosition5+(bmpApple.getWidth()/2)) &&
			((yFromTop2+(bmpApple.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
			((x+bmpBucket.getWidth()/2)>dropPosition5+(bmpApple.getWidth()/2))&&
			(yFromTop2+(bmpApple.getHeight()/3))<=height-10) ||
						   
			((x-(bmpBucket.getWidth()/2)<dropPosition5+(bmpCoconut.getWidth()/2)) &&
			((yFromTop2+(bmpCoconut.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
			((x+bmpBucket.getWidth()/2)>dropPosition5+(bmpCoconut.getWidth()/2))&&
			(yFromTop2+(bmpCoconut.getHeight()/3))<=height-10) ||
						   
			((x-(bmpBucket.getWidth()/2)<dropPosition5+(bmpOrange.getWidth()/2)) &&
			 ((yFromTop2+(bmpOrange.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
			 ((x+bmpBucket.getWidth()/2)>dropPosition5+(bmpOrange.getWidth()/2))&&
			 (yFromTop2+(bmpOrange.getHeight()/3))<=height-10) ||
						    
			((x-(bmpBucket.getWidth()/2)<dropPosition5+(bmpBanana.getWidth()/2)) &&
			 ((yFromTop2+(bmpBanana.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
			 ((x+bmpBucket.getWidth()/2)>dropPosition5+(bmpBanana.getWidth()/2))&&
			 (yFromTop2+(bmpBanana.getHeight()/3))<=height-10) ||
						     
			((x-(bmpBucket.getWidth()/2)<dropPosition5+(bmpMelon.getWidth()/2)) &&
			((yFromTop2+(bmpMelon.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
			((x+bmpBucket.getWidth()/2)>dropPosition5+(bmpMelon.getWidth()/2))&&
			(yFromTop2+(bmpMelon.getHeight()/3))<=height-10)
					      					  
			)
									
		{
		if(ismusic)
			dropInBucketMusic.start();
			successDrop4=true;
			score+=15;
			yFromTop2= height/130;
			f+=1;
			test=true;
			test1=true;
			
		}	
	
	//************************************************************************************//
	//											END										  //
	//************************************************************************************//
		
		if(test && f%3==0)
			{
			Log.d("hiii", "f"+f);	
				lifeEndChecker=true;
				test=false;
			}
		
		/////////////////////////////////////////////////////////Change
		if(test1 && score>0 &&score%150==0 && ySpeed<14)
		{
			ySpeed+=1;
			ySpeed2+=1;
			ySpeed3+=.5;
			test1 = false;
			Log.d("", "SPEED CHECKER "+ySpeed );
		}
	
		// POWER BOOSTER Concept
		
		if(powerDrop)
		{
			powerCase=randomPos1.nextInt(4); 	// Drop Position of powers.......
			powerDrop=false;
		}
	
		if(ypowerTop< height-bmpBucket.getHeight()/2 && lifeEndChecker)
		{
			switch(powerCase)
			{
				case 0:
					checking();
						canvas.drawBitmap(bmpLifeUp, dropPosition2,ypowerTop, null);
						bmp = bmpLifeUp;
							break;
				case 1:
					checking();
						canvas.drawBitmap(bmpLifeDown, dropPosition2, ypowerTop, null);
						bmp = bmpLifeDown;
							break;
				case 2:
					checking();
						canvas.drawBitmap(bmpSpeedUp, dropPosition2, ypowerTop, null);
						bmp = bmpSpeedUp;
							break;
				case 3:
					checking();
						canvas.drawBitmap(bmpScoreMinus, dropPosition2, ypowerTop, null);
						bmp = bmpScoreMinus;
							break;
					
			}
			ypowerTop+=ySpeed3;
		}
		else
		{
//			ypowerTop=0;
			ypowerTop=width/5;
			powerDrop=true;
			lifeEndChecker=false;
			successDrop2 = true;
			
		}
	
	// Checking For Life Up
		
		if( ((x-(bmpBucket.getWidth()/2)<dropPosition2+(bmpLifeUp.getWidth()/2)) &&
				((ypowerTop+(bmpLifeUp.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
				((x+bmpBucket.getWidth()/2)>dropPosition2+(bmpLifeUp.getWidth()/2))&&
				(ypowerTop+(bmpLifeUp.getHeight()/3))<=height-10) && bmp == bmpLifeUp )
		{
			if(ismusic)
				dropInBucketMusic.start();
			if(lifeCounter<3)
				lifeCounter++;
//			ypowerTop=0;		
			ypowerTop=width/5;
			lifeEndChecker=false;
			successDrop2 = true;
		}
		
	// Checking For Life Down
		
		else if( ((x-(bmpBucket.getWidth()/2)<dropPosition2+(bmpLifeDown.getWidth()/2)) &&
				((ypowerTop+(bmpLifeDown.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
				((x+bmpBucket.getWidth()/2)>dropPosition2+(bmpLifeDown.getWidth()/2))&&
				(ypowerTop+(bmpLifeDown.getHeight()/3))<=height-10)&& bmp == bmpLifeDown )
		{
			if(ismusic)
				dropInBucketMusic.start();
			if(lifeCounter>0)
				lifeCounter--;
//			ypowerTop=0;		
			ypowerTop=width/5;
			lifeEndChecker=false;
			successDrop2 = true;
		}
		
	// Checking For Speed Up 
		
		else if( ((x-(bmpBucket.getWidth()/2)<dropPosition2+(bmpSpeedUp.getWidth()/2)) &&
				((ypowerTop+(bmpSpeedUp.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
				((x+bmpBucket.getWidth()/2)>dropPosition2+(bmpSpeedUp.getWidth()/2))&&
				(ypowerTop+(bmpSpeedUp.getHeight()/3))<=height-10) &&bmp == bmpSpeedUp)
		{
			if(ismusic)
				dropInBucketMusic.start();
			ySpeed = ySpeed+1;
			ySpeed2 = ySpeed2+1;
			ypowerTop=0;	
			ypowerTop=width/5;
			lifeEndChecker=false;
			successDrop2 = true;
		}
		
	// Checking For Score Minus
		
		else if( ((x-(bmpBucket.getWidth()/2)<dropPosition2+(bmpScoreMinus.getWidth()/2)) &&
				((ypowerTop+(bmpScoreMinus.getHeight()/3))>=height-(bmpBucket.getHeight()+10))&&
				((x+bmpBucket.getWidth()/2)>dropPosition2+(bmpScoreMinus.getWidth()/2))&&
				(ypowerTop+(bmpScoreMinus.getHeight()/3))<=height-10) &&bmp == bmpScoreMinus)
		{
			if(ismusic)
				dropInBucketMusic.start();
			if(score>0)
				score = score-15;
			ypowerTop=0;	
			ypowerTop=width/5;
			lifeEndChecker=false;
			successDrop2 = true;
		}
	
		
	// Creating Bundle for score
		
		String strCheck=String.valueOf(score);
		Bundle gameScore=new Bundle();
		gameScore.putString("PlayerScore_Medium",strCheck);		            
	
		//Checking status of life of Game
		if(lifeCounter==4)
			canvas.drawBitmap(bmpFour, width-bmpThree.getWidth()-3, bmpStone.getHeight()/5, null);								   
		if(lifeCounter==3)
			canvas.drawBitmap(bmpThree, width-bmpThree.getWidth()-3,bmpStone.getHeight()/5, null);
		if(lifeCounter==2)
			canvas.drawBitmap(bmpTwo, width-bmpThree.getWidth()-3,bmpStone.getHeight()/5, null);
		if(lifeCounter==1)
			canvas.drawBitmap(bmpOne, width-bmpThree.getWidth()-3,bmpStone.getHeight()/5, null);
		if(lifeCounter==0)
		{
			canvas.drawBitmap(bmpZero, width-bmpThree.getWidth()-3,bmpStone.getHeight()/5, null);

			lifeCounter = 1;


//					FruitBucketEasyMode.this.finish();
			paused = false;
			Intent intent=new Intent(FruitBucketMediumMode.this,SaveMe.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtras(gameScore);
			intent.putExtra("id", 2);                                        // TESTING
			startActivity(intent);;
	}									

	//Applying  Touch Concept on Bucket
								
	if(x==0 && y==0)
	{
		canvas.drawBitmap(bmpBucket,width/2-bmpBucket.getWidth()/2,height-(bmpBucket.getHeight()+10), null);
	}
	
	if(x!=0 && y!=0)
	{				    
		if(counter>=9000)
		{
			// Checking Both Left and Right Collision of Bucket With walls

			if(x<=bmpBucket.getWidth()/2 || x>=width-bmpBucket.getWidth()/2)
			{

				if(x<=bmpBucket.getWidth()/2) 		// Left Wall collision 
				{
					x=bmpBucket.getWidth()/2; 
				}					        	

				if(x>=width-bmpBucket.getWidth()/2) // Right Wall Collision
				{
					x=width-bmpBucket.getWidth()/2;
				}


				canvas.drawBitmap(bmpBucket,x-(bmpBucket.getWidth()/2),height-(bmpBucket.getHeight()+10), null);
			}
			
			else
			{
				canvas.drawBitmap(bmpBucket,x-(bmpBucket.getWidth()/2),height-(bmpBucket.getHeight()+10), null);
			}
		}			      

		else
		{
			if(x <= bmpBucket.getWidth()/2  ||   x>=width-bmpBucket.getWidth()/2)
			{
				if(x<=bmpBucket.getWidth()/2)
				{
					x=bmpBucket.getWidth()/2; 
				}

				if(x>=width-bmpBucket.getWidth()/2)
				{
					x=width-bmpBucket.getWidth()/2;
				}

				canvas.drawBitmap(bmpBucket,x-(bmpBucket.getWidth()/2),height-(bmpBucket.getHeight()+10), null);
			}
			else
			{
				canvas.drawBitmap(bmpBucket,x-(bmpBucket.getWidth()/2),height-(bmpBucket.getHeight()+10), null);
			}

		} 
	}
	
canvas.drawText(scoreUpdate(),bmpSCard.getWidth()+5, bmpSCard.getHeight(), paintmyscore);				

holder.unlockCanvasAndPost(canvas);					
			}			
		}
		
	}
}