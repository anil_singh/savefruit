package alienapps.com.perkgame.savefruit;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import alienapps.com.perkgame.R;

public class CreditForGame extends Activity
{
	TextView tv1,tv2,tv3,tv4;
	ImageView ivCredit,ivIcon,ivGraphicDesign,ivcserd;
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
							 WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.creditforgame);
		
		ivIcon=(ImageView)findViewById(R.id.creditIVIcon);
		ivCredit=(ImageView)findViewById(R.id.creditIV);
		ivGraphicDesign=(ImageView)findViewById(R.id.creditIVgdesign);
		ivcserd=(ImageView)findViewById(R.id.cserd);
		
		tv1=(TextView)findViewById(R.id.creditTV1);
		tv2=(TextView)findViewById(R.id.creditTV2);
		tv3=(TextView)findViewById(R.id.creditTV3);
		//tv4=(TextView)findViewById(R.id.creditTV4);
		
		
		Animation creditanim= AnimationUtils.loadAnimation(this, R.anim.creditanimation);
		
		tv1.setText("Anil Singh");
		tv2.setText("Ravi Shanker");
		tv3.setText("Anil Singh");
		
		ivIcon.startAnimation(creditanim);
		ivCredit.startAnimation(creditanim);
		
		tv1.startAnimation(creditanim);
		tv2.startAnimation(creditanim);
		
		ivGraphicDesign.startAnimation(creditanim);
		tv3.startAnimation(creditanim);
		
		ivcserd.startAnimation(creditanim);
		
	}
}
