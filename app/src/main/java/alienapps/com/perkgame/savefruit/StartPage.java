package alienapps.com.perkgame.savefruit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.perk.perksdk.PerkManager;

import alienapps.com.perkgame.R;

public class StartPage extends Activity {

    Vibrator vib;
    SharedPreferences prefer;
    Editor editor;
    ImageButton imb;
    String filename[] = {"ScoreCardFile", "ScoreCardFile_Medium", "ScoreCardFile_Hard"};
    String mode[] = {"Easy_mode", "Medium_Mode", "Difficult_Mode"};
    int lbrId[] = {12924, 12926, 12928};
    int score, i = 0;
    ImageView moreAppBox;

    String api_key = "ff988bf37f70796b256abf405ea35d2c9dfbc43f";

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.startpage);
        PerkManager.startSession(StartPage.this, api_key);

        vib = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        prefer = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        imb = (ImageButton) findViewById(R.id.imgBtnMoreApps);
        if (prefer.getBoolean("IsMusic", true))
            imb.setImageResource(R.mipmap.soundon);
        else
            imb.setImageResource(R.mipmap.soundoff);


        moreAppBox = (ImageView) findViewById(R.id.moreapp_box);

        Animation boxBlink = AnimationUtils.loadAnimation(StartPage.this, R.anim.blink);
        moreAppBox.startAnimation(boxBlink);
//        if (PerkManager.1)
        PerkManager.launchLoginPage(StartPage.this);
        String username = PerkManager.getAppsaholicUsername();
        Toast.makeText(this, username, Toast.LENGTH_LONG).show();

    }


    @Override
    protected void onRestoreInstanceState(Bundle restoreBundle) {
        super.onRestoreInstanceState(restoreBundle);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        finish();
        System.exit(0);
        //		dialoBuilder();

    }

    public void buttonMethod(View v) {
        switch (v.getId()) {
            case R.id.imgBtn1:
                vib.vibrate(27);
                startActivity(new Intent(StartPage.this, LevelPage.class));
                break;

            case R.id.imgBtn2:
                vib.vibrate(27);
                //				startActivity(new Intent(StartPage.this,HighScore.class));
                break;

            case R.id.imgBtn3:
                vib.vibrate(27);
                startActivity(new Intent(StartPage.this, Instruction.class));
                break;

            case R.id.imgBtn4:
                vib.vibrate(27);
                finish();
                System.exit(0);
                break;

            case R.id.imgBtnCredit:
                vib.vibrate(27);
                startActivity(new Intent(StartPage.this, CreditForGame.class));
                break;

            case R.id.imgBtnMoreApps:
                if (prefer.getBoolean("IsMusic", true)) {
                    imb.setImageResource(R.mipmap.soundoff);
                    editor = prefer.edit();
                    editor.putBoolean("IsMusic", false);
                } else {
                    imb.setImageResource(R.mipmap.soundon);
                    editor = prefer.edit();
                    editor.putBoolean("IsMusic", true);
                }
                editor.commit();
            /*Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("market://search?q=pub:CSERD"));
				startActivity(intent);*/
                break;

            case R.id.moreapp_box:

                PerkManager.showPortal(StartPage.this, api_key);

                break;
            case R.id.getrewards:

                PerkManager.launchDataPoints(StartPage.this);

                break;
        }
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            //			NetworkInfo[] info = connectivity.getAllNetworkInfo();
            NetworkInfo netInfo = connectivity.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            }
        }
        return false;
    }
}
