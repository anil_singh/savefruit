package alienapps.com.perkgame.savefruit;

import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.perk.perksdk.PerkManager;

import alienapps.com.perkgame.R;

public class GameOver extends Activity implements OnClickListener
{
	TextView bundleTxtViewScore,bundleTxtViewHighScore;
	String TAG = "GameOver";
	String receiveScore;
//	ImageButton okIBtn;
	ImageView iv_Restart,iv_fbShare,iv_Menu;
	String mode;
	public static final String filename[]= {"ScoreCardFile", "ScoreCardFile_Medium", "ScoreCardFile_Hard"};
	SharedPreferences scoreData;
	String saveDataString;
	String prevScore,currScore;
	String[] hScore = new String[3];	
	byte[] data;
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);

		Bundle receiveBundle=getIntent().getExtras();
		int id = receiveBundle.getInt("id");

		switch (id) 
		{
		case 1:
			Log.d(TAG, "onCreate() called with: " + "savedInstanceState = [" + id + "]");
			setContentView(R.layout.gameovereasy);

			iv_Restart=(ImageView)findViewById(R.id.iv_Restart);
//			iv_fbShare=(ImageView)findViewById(R.id.iv_fbShare);
			iv_Menu=(ImageView)findViewById(R.id.iv_Menu);
			
			
			receiveScore=receiveBundle.getString("PlayerScore");
			
			bundleTxtViewScore=(TextView)findViewById(R.id.curentScore);
		    bundleTxtViewHighScore=(TextView)findViewById(R.id.highScore);
			bundleTxtViewScore.setText(receiveScore);
			
			scoreData=getSharedPreferences(filename[0], 0);	
			
			mode="Easy_Mode";
			scoreMethod(id);	
			
			iv_Restart.setOnClickListener(this);
			iv_Menu.setOnClickListener(this);
//			shareit();
			
			break;

		case 2:
			setContentView(R.layout.gameovermedium);
			iv_Restart=(ImageView)findViewById(R.id.iv_Restart);
//			iv_fbShare=(ImageView)findViewById(R.id.iv_fbShare);
			iv_Menu=(ImageView)findViewById(R.id.iv_Menu);
			
			receiveScore=receiveBundle.getString("PlayerScore_Medium");
			bundleTxtViewScore=(TextView)findViewById(R.id.curentScore_medium);
			bundleTxtViewHighScore=(TextView)findViewById(R.id.highScore_medium);	
			
			bundleTxtViewScore.setText(receiveScore);
			scoreData=getSharedPreferences(filename[1], 0);			
			
			mode="Medium_Mode";
			scoreMethod(id);	
			
//			iv_fbShare.setOnClickListener(this);
			iv_Restart.setOnClickListener(this);
			iv_Menu.setOnClickListener(this);
			
			break;
		case 3:
			setContentView(R.layout.gameoverhard);

			iv_Restart=(ImageView)findViewById(R.id.iv_Restart);
//			iv_fbShare=(ImageView)findViewById(R.id.iv_fbShare);
			iv_Menu=(ImageView)findViewById(R.id.iv_Menu);
			
			receiveScore=receiveBundle.getString("PlayerScore_Hard");
			
			bundleTxtViewScore=(TextView)findViewById(R.id.curentScore_hard);
			bundleTxtViewHighScore=(TextView)findViewById(R.id.highScore_hard);		
			bundleTxtViewScore.setText(receiveScore);
			
			scoreData=getSharedPreferences(filename[2], 0);			
			
			mode="Difficult_Mode";
			scoreMethod(id);		
			
//			iv_fbShare.setOnClickListener(this);
			iv_Restart.setOnClickListener(this);
			iv_Menu.setOnClickListener(this);
			
			break;
			
		case 4:
			finish();
			/*scoreData = getSharedPreferences(filename[0], 0);
			hScore[0] = scoreData.getString("SPEditorKey","0");
			
			scoreData = getSharedPreferences(filename[1], 0);
			hScore[1] = scoreData.getString("SPEditorKey","0");
			
			scoreData = getSharedPreferences(filename[2], 0);
			hScore[2] = scoreData.getString("SPEditorKey","0");
			
			Intent in = new Intent();
			in.putExtra("HScore", hScore);
			setResult(RESULT_OK, in);*/
		}
			
			
//			registeringID();
			
//			Bundle receiveBundle=getIntent().getExtras();			
							   
						
	}
	
	private void scoreMethod(int i) 
	{

		//Checking The status of High Score					
		scoreData=getSharedPreferences(filename[i-1],0);
		saveDataString=scoreData.getString("SPEditorKey","0");
		Log.d("", "Value of "+saveDataString);
		if(Integer.parseInt(receiveScore)>Integer.parseInt(saveDataString))
		{
			currScore = receiveScore;
			if((i-1)==0)
			{
			}
			if((i-1)==1)
			{
			}
			if((i-1)==2)
			{
			}
			bundleTxtViewHighScore.setText(currScore);
			String msg = " Congrats!!! Your got HighScore ";
			onTost(msg);
		}
		else
		{
			currScore = saveDataString;
			bundleTxtViewHighScore.setText(currScore);
		}

		// Setting the Score		
		Editor editor=scoreData.edit();
		editor.putString("SPEditorKey", currScore);
		editor.commit();			
	}
	
	@Override
	public void finish()
	{
		scoreData = getSharedPreferences(filename[0], 0);
		hScore[0] = scoreData.getString("SPEditorKey","0");
		
		scoreData = getSharedPreferences(filename[1], 0);
		hScore[1] = scoreData.getString("SPEditorKey","0");
		
		scoreData = getSharedPreferences(filename[2], 0);
		hScore[2] = scoreData.getString("SPEditorKey","0");
		
		Intent in = new Intent();
		in.putExtra("HScore", hScore);
		setResult(RESULT_OK, in);
	 
	  super.finish();
	} 
	
	@Override
	public void onBackPressed()
	{
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, LevelPage.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		super.onBackPressed();
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	@Override
	public void onClick(View v) 
	{
//		PerkManager.trackEvent(GameOver.this, event_id, "5902d296dee5aa8c07e0151e3709f4fc540e363c", true, null);
		// TODO Auto-generated method stub
		
		switch(v.getId())
		{
		
		/*case R.id.iv_fbShare:
//			new ShareScore(GameOver.this, receiveScore, mode).fbshare();
			if(mode.equalsIgnoreCase("Easy_Mode"))
				shareit(R.id.easy);
			else if(mode.equalsIgnoreCase("Easy_Mode"))
				shareit(R.id.h);
			shareit();
			break;*/
		case R.id.iv_Menu:
			Intent in = new Intent(GameOver.this,LevelPage.class);
			in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(in);
			break;
			
		case R.id.iv_Restart:
			if(mode.equalsIgnoreCase("Easy_Mode"))
			{
				Intent intent2 = new Intent(GameOver.this,FruitBucketEasyMode.class);
				intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent2);
			}
			else if(mode.equalsIgnoreCase("Medium_Mode"))
			{
				Intent intent3 = new Intent(GameOver.this,FruitBucketMediumMode.class);
				intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent3);
			}

			else if(mode.equalsIgnoreCase("Difficult_Mode"))
			{
				Intent intent4 = new Intent(GameOver.this,FruitBucketHardMode.class);
				intent4.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent4);
			}
			
			break;
			
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
	  super.onActivityResult(requestCode, resultCode, data);
	}

	public void shareit()
	{
		View view =  findViewById(R.id.easy);
		try 
		{
			view.getRootView();
			view.setDrawingCacheEnabled(true);
			view.buildDrawingCache(true);
			Bitmap bitmap = view.getDrawingCache(true);
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			if(bitmap!=null)
			{
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				byte[] data1 = stream.toByteArray();
				data = new byte[data1.length];
				for (int i = 0; i < data1.length; i++) 
				{
					data[i] = data1[i];
				}
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		view.destroyDrawingCache();
//		new ShareScore(GameOver.this, receiveScore, mode, data).fbshare();
	}

	public void onTost(String msg) 
	{
		LayoutInflater lyIn = getLayoutInflater();
		View layout = lyIn.inflate(R.layout.custum_tost, (ViewGroup)findViewById(R.id.toast_custom));
		TextView txtvw = (TextView) layout.findViewById(R.id.tvtoast);
		txtvw.setText(msg);
		txtvw.setTextColor(Color.WHITE);
		txtvw.setGravity(Gravity.CENTER);
		Toast tost = new Toast(getApplicationContext());
		tost.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		tost.setDuration(Toast.LENGTH_SHORT);
		tost.setView(layout);
		tost.show();
	}
	
}
