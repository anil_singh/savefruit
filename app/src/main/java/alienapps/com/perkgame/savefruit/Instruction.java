package alienapps.com.perkgame.savefruit;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import alienapps.com.perkgame.R;

public class Instruction extends Activity
{
	TextView tvContent;
	String strContent,strContent2;
	String strLifeUp,strSpeedUp,strScoreMinus,strLifeDown;
	TextView txtViewForBooster;
	ImageView imgViewForBooster;

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.gameinstruction);

		//		AdView ad=(AdView)findViewById(R.id.adss);
		//		ad.loadAd(new AdRequest());

		tvContent=(TextView)findViewById(R.id.tvcontent);
		txtViewForBooster=(TextView)findViewById(R.id.tvOfBooster);
		imgViewForBooster=(ImageView)findViewById(R.id.imgViewOfBooster);

		strContent="Welcome to Save Fruit Game.\n" +
				"This game is very easy to play. Only you have to concentrate on the fruit and catch in the bucket\n" +
				"For game player we have given them Power boosters, so that they can enjoy game and can score high\n" +
				"Power Boosters Detail is mentioned below. You can review this powers.";
		//Initializing Strings with data

		strLifeUp="If You Catch this, It will add one extra life to Your Game";
		strSpeedUp="If You Catch this, It will increase the speed of fruits, falling towards Bucket";
		strScoreMinus="It will Decrease your score, as per the mode of difficulty you have choosed";
		strLifeDown="If You Catch this, It will cost you one Life of Your Game, so Beware of This";


		tvContent.setText(strContent);
		imgViewForBooster.setImageResource(R.mipmap.lifeu2p);
		txtViewForBooster.setText(strLifeUp);
	}

	public void instructionTouchMethod(View v)
	{
		switch(v.getId())
		{
		case R.id.imgViewLifeUp:
			imgViewForBooster.setImageResource(R.mipmap.lifeu2p);
			txtViewForBooster.setText(strLifeUp);
			break;
		case R.id.imgViewSpeedup:
			imgViewForBooster.setImageResource(R.mipmap.speedup);
			txtViewForBooster.setText(strSpeedUp);
			break;
		case R.id.imgViewScoreminus:
			imgViewForBooster.setImageResource(R.mipmap.scoreminus);
			txtViewForBooster.setText(strScoreMinus);
			break;
		case R.id.imgViewBomb:
			imgViewForBooster.setImageResource(R.mipmap.bomb);
			txtViewForBooster.setText(strLifeDown);
			break;
		}

	}
}
