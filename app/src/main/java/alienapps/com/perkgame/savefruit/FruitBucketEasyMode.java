package alienapps.com.perkgame.savefruit;

import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;


import com.perk.perksdk.PerkInterface;
import com.perk.perksdk.PerkListener;
import com.perk.perksdk.PerkManager;

import alienapps.com.perkgame.R;

public class FruitBucketEasyMode extends Activity implements OnTouchListener {
    GraphicBucketClass graphicBC;

    Bitmap bmpForest, bmpBucket, bmpStone, bmpSCard, bmpLife, bmpLifeUp, bmpFullBucket, bmpEmptyBuket;
    Bitmap bmpZero, bmpOne, bmpTwo, bmpThree, bmpFour;
    Bitmap bmpApple, bmpCoconut, bmpOrange, bmpMango, bmpBanana, bmpMelon;
    Bitmap bmpLifeDown, bmpSpeedUp, bmpScoreMinus;
    boolean paused = true;
    int width;
    int height;
    int counter = 0, f = 0, speedFlag1 = 1, speedFlag2 = 2, speedFlag3 = 3;
    float x, y;
    public static int scoreCounter = 0;
    static int count = 1;
    String strScore = null;
    Display display;
    MediaPlayer dropInBucketMusic, gameBgMusic;
    boolean test = false;
    boolean test1 = false;
    SharedPreferences pref;
    boolean ismusic;
    String TAG = "FruitBucketEasyMode";
    String event_id = "df134859d8e6f27e73cf4a3d8e534cc2e647c86b";

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        graphicBC = new GraphicBucketClass(this);
        graphicBC.setOnTouchListener(this);

        registerID(); // Registering all ID of XML Layout

        x = y = 0;
        pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        ismusic = pref.getBoolean("IsMusic", true);
        Log.d("", "Music on " + ismusic);
        if (ismusic) {
            dropInBucketMusic = MediaPlayer.create(FruitBucketEasyMode.this, R.raw.dropinbuckmusic);
            gameBgMusic = MediaPlayer.create(FruitBucketEasyMode.this, R.raw.mainbgmusic);
            gameBgMusic.start();
            gameBgMusic.setLooping(true);
        }

        setContentView(graphicBC);
    }

    public void registerID() {
        bmpForest = BitmapFactory.decodeResource(getResources(), R.mipmap.bgimg2);
        bmpStone = BitmapFactory.decodeResource(getResources(), R.mipmap.stonewall);

        bmpStone = Bitmap.createScaledBitmap(bmpStone, width, bmpStone.getHeight(), false);

        bmpSCard = BitmapFactory.decodeResource(getResources(), R.mipmap.scard2);
        bmpLife = BitmapFactory.decodeResource(getResources(), R.mipmap.lifee);
        bmpEmptyBuket = BitmapFactory.decodeResource(getResources(), R.mipmap.bucket3);
        bmpForest = Bitmap.createScaledBitmap(bmpForest, width, height, false);
        bmpFullBucket = BitmapFactory.decodeResource(getResources(), R.mipmap.fullbucket);


        // Bitmap Images of Fresh Fruits

        bmpApple = BitmapFactory.decodeResource(getResources(), R.mipmap.apple);
        bmpOrange = BitmapFactory.decodeResource(getResources(), R.mipmap.orange);
        bmpCoconut = BitmapFactory.decodeResource(getResources(), R.mipmap.coco);
        bmpMango = BitmapFactory.decodeResource(getResources(), R.mipmap.mango);
        bmpBanana = BitmapFactory.decodeResource(getResources(), R.mipmap.banana);
        bmpMelon = BitmapFactory.decodeResource(getResources(), R.mipmap.mealon);

        //Bitmap Images for Numbers

        bmpZero = BitmapFactory.decodeResource(getResources(), R.mipmap.zeroo);
        bmpOne = BitmapFactory.decodeResource(getResources(), R.mipmap.onee);
        bmpTwo = BitmapFactory.decodeResource(getResources(), R.mipmap.twoo);
        bmpThree = BitmapFactory.decodeResource(getResources(), R.mipmap.threee);
        bmpFour = BitmapFactory.decodeResource(getResources(), R.mipmap.fourr);

        //Introducing Music in Game
        //dropInBucketMusic=MediaPlayer.create(FruitBucketEasyMode.this, R.raw.dropinbucket);
        //gameBgMusic=MediaPlayer.create(FruitBucketEasyMode.this,R.raw.islnd_fvr_bgmusic);

        //Power Boost

        bmpScoreMinus = BitmapFactory.decodeResource(getResources(), R.mipmap.scoreminus);
        bmpSpeedUp = BitmapFactory.decodeResource(getResources(), R.mipmap.speedup);
        bmpLifeDown = BitmapFactory.decodeResource(getResources(), R.mipmap.bomb);
        bmpLifeUp = BitmapFactory.decodeResource(getResources(), R.mipmap.lifeu2p);


    }

    @Override
    protected void onPause() {
        super.onPause();
        graphicBC.pause();
        if (graphicBC.lifeCounter != 0 && paused) {
            Intent intent = new Intent(FruitBucketEasyMode.this, PauseResumeStop.class);
            intent.putExtra("id", 1);
            startActivity(intent);
            paused = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ismusic) {
            dropInBucketMusic.release();
            gameBgMusic.release();
            dropInBucketMusic = MediaPlayer.create(FruitBucketEasyMode.this, R.raw.dropinbuckmusic);
            gameBgMusic = MediaPlayer.create(FruitBucketEasyMode.this, R.raw.mainbgmusic);
            dropInBucketMusic.start();
            gameBgMusic.start();
            gameBgMusic.setLooping(true);
        }

        graphicBC.resume();
        paused = true;
    }

    public boolean onTouch(View v, MotionEvent event) {
        x = event.getX();
        y = event.getY();

        return true;
    }

    public void onBackPressed() {
        if (paused) {
            paused = false;
            Intent intent = new Intent(FruitBucketEasyMode.this, PauseResumeStop.class);
            intent.putExtra("id", 1);
            startActivity(intent);
        }

    }

    // Creating the new Graphic bucket Class

    class GraphicBucketClass extends SurfaceView implements Runnable {
        SurfaceHolder holder;
        Thread mythread = null;
        boolean isItOk = false;

        int dropPosition1, dropPosition2, fruitCase, dropPosition4, lifeDroper;
        int c = 0, score = 0;
        int lifeCounter = 3, fruitCounter = 0;

        int yFromTop = width / 5;
        int ypowerTop = width / 5;

        float ySpeed = height / 110;
        float ySpeed2 = height / 130;
        ///////////////////////////////////////////////////change
        //		    float x = 110;

        boolean successDrop1 = true, successDrop2 = true, successDrop3 = true, successDrop4 = true;
        boolean fresh = true;
        boolean unfresh = false;
        boolean lifeEndChecker = false;
        protected final Paint paintmyscore = new Paint();
        int zero = 0;
        private boolean powerDrop = true;
        private int powerCase;
        Bitmap bmp;

        public GraphicBucketClass(Context context) {
            super(context);
            holder = getHolder();
            mythread = new Thread(this);
            mythread.start();
        }

        public void pause() {
            isItOk = false;
            //		   dropInBucketMusic.stop();
            if (ismusic)
                gameBgMusic.stop();
            while (true) {
                try {
                    mythread.join();
                    mythread.interrupt();

                } catch (Exception e) {
                }

                break;
            }
        }

        public void resume() {
            isItOk = true;
            mythread = new Thread(this);
            mythread.start();
            //			dropInBucketMusic.start();
            //			gameBgMusic.start();

        }

        // Introducing Score Counting Method
        public String scoreUpdate() {
            strScore = " " + score;
            return strScore;
        }

        //Controlling and Checking the fruits to get out of the window frame
        public void checking() {
            if (dropPosition1 < 20) {
                dropPosition1 = dropPosition1 + 10;
                successDrop1 = true;
            } else if (dropPosition1 > width - bmpApple.getWidth()) {
                dropPosition1 = dropPosition1 - bmpApple.getWidth();
                successDrop1 = true;
            }
            if (dropPosition2 < 20) {
                dropPosition2 = dropPosition2 + 10;
                successDrop2 = true;
            } else if (dropPosition2 > width - bmpApple.getWidth()) {
                dropPosition2 = dropPosition2 - bmpApple.getWidth();
                successDrop2 = true;
            }
        }

        public void run() {
            while (isItOk) {
                if (!holder.getSurface().isValid())
                    continue;

                if (score % 100 == 0 && score != 0) {
                    bmpBucket = bmpFullBucket;
                } else {
                    bmpBucket = bmpEmptyBuket;
                }

                Canvas canvas = holder.lockCanvas();

				/*  canvas.drawBitmap(bmpForest, 0, 0, null);
                canvas.drawBitmap(bmpStone,0,height-bmpStone.getHeight(),null);
				canvas.drawBitmap(bmpSCard, 0, height-bmpSCard.getHeight()-5,null);
				canvas.drawBitmap(bmpLife, width-3*(bmpLife.getWidth()/2),height-bmpLife.getHeight()-8,null); */

                canvas.drawBitmap(bmpForest, 0, 0, null);
                canvas.drawBitmap(bmpStone, 0, 0, null);
                canvas.drawBitmap(bmpSCard, 0, bmpStone.getHeight() / 6, null);
                canvas.drawBitmap(bmpLife, width - 3 * (bmpLife.getWidth() / 2), bmpStone.getHeight() / 4, null);

                //!!!  Painting the score on Canvas !!!//

                paintmyscore.setColor(Color.GREEN);
                paintmyscore.setStyle(Style.FILL);
                paintmyscore.setTextSize(height / 20);

                //Declaring random variables//

                Random randomPos1 = new Random();

                if (successDrop1) {
                    dropPosition1 = randomPos1.nextInt(width - bmpMelon.getWidth());
                    successDrop1 = false;
                }

                if (successDrop2) {
                    dropPosition2 = randomPos1.nextInt(width - bmpMelon.getWidth());
                    successDrop2 = false;
                }
                if (successDrop3) {
                    fruitCase = randomPos1.nextInt(6);    // Drop Position of Fruits.......
                    //							dropPosition4=randomPos1.nextInt(width); // Random Drop For UnFresh Fruits
                    successDrop3 = false;
                }

                switch (fruitCase) {
                    case 0:
                        checking();
                        canvas.drawBitmap(bmpApple, dropPosition1, yFromTop, null);
                        break;
                    case 1:
                        checking();
                        canvas.drawBitmap(bmpCoconut, dropPosition1, yFromTop, null);
                        break;
                    case 2:
                        checking();
                        canvas.drawBitmap(bmpOrange, dropPosition1, yFromTop, null);
                        break;
                    case 3:
                        checking();
                        canvas.drawBitmap(bmpMango, dropPosition1, yFromTop, null);
                        break;
                    case 4:
                        checking();
                        canvas.drawBitmap(bmpBanana, dropPosition1, yFromTop, null);
                        break;
                    case 5:
                        checking();
                        canvas.drawBitmap(bmpMelon, dropPosition1, yFromTop, null);
                        break;

                }

                //speedForDifferentMobile();

                //						if(yFromTop < height-(5*(bmpBucket.getHeight()))/3)
                //						if(yFromTop < height-bmpBucket.getHeight()-5)
                if (yFromTop < height - bmpBucket.getHeight() / 2) {
                    yFromTop += ySpeed;
                    successDrop1 = false;
                    //							ypowerTop+=ySpeed;
                } else {
                    lifeCounter--;
                    yFromTop = width / 5;
                    successDrop1 = true;
                    successDrop3 = true;
                }

                // Finding out the collision between Bucket and fruits

                if (((x - (bmpBucket.getWidth() / 2) < dropPosition1 + (bmpMango.getWidth() / 2)) &&
                        ((yFromTop + (bmpMango.getHeight() / 3)) >= height - (bmpBucket.getHeight() + 10)) &&
                        ((x + bmpBucket.getWidth() / 2) > dropPosition1 + (bmpMango.getWidth() / 2)) &&
                        (yFromTop + (bmpMango.getHeight() / 3)) <= height - 10) ||

                        ((x - (bmpBucket.getWidth() / 2) < dropPosition1 + (bmpApple.getWidth() / 2)) &&
                                ((yFromTop + (bmpApple.getHeight() / 3)) >= height - (bmpBucket.getHeight() + 10)) &&
                                ((x + bmpBucket.getWidth() / 2) > dropPosition1 + (bmpApple.getWidth() / 2)) &&
                                (yFromTop + (bmpApple.getHeight() / 3)) <= height - 10) ||

                        ((x - (bmpBucket.getWidth() / 2) < dropPosition1 + (bmpCoconut.getWidth() / 2)) &&
                                ((yFromTop + (bmpCoconut.getHeight() / 3)) >= height - (bmpBucket.getHeight() + 10)) &&
                                ((x + bmpBucket.getWidth() / 2) > dropPosition1 + (bmpCoconut.getWidth() / 2)) &&
                                (yFromTop + (bmpCoconut.getHeight() / 3)) <= height - 10) ||

                        ((x - (bmpBucket.getWidth() / 2) < dropPosition1 + (bmpOrange.getWidth() / 2)) &&
                                ((yFromTop + (bmpOrange.getHeight() / 3)) >= height - (bmpBucket.getHeight() + 10)) &&
                                ((x + bmpBucket.getWidth() / 2) > dropPosition1 + (bmpOrange.getWidth() / 2)) &&
                                (yFromTop + (bmpOrange.getHeight() / 3)) <= height - 10) ||

                        ((x - (bmpBucket.getWidth() / 2) < dropPosition1 + (bmpBanana.getWidth() / 2)) &&
                                ((yFromTop + (bmpBanana.getHeight() / 3)) >= height - (bmpBucket.getHeight() + 10)) &&
                                ((x + bmpBucket.getWidth() / 2) > dropPosition1 + (bmpBanana.getWidth() / 2)) &&
                                (yFromTop + (bmpBanana.getHeight() / 3)) <= height - 10) ||

                        ((x - (bmpBucket.getWidth() / 2) < dropPosition1 + (bmpMelon.getWidth() / 2)) &&
                                ((yFromTop + (bmpMelon.getHeight() / 3)) >= height - (bmpBucket.getHeight() + 10)) &&
                                ((x + bmpBucket.getWidth() / 2) > dropPosition1 + (bmpMelon.getWidth() / 2)) &&
                                (yFromTop + (bmpMelon.getHeight() / 3)) <= height - 10)

                        )

                {
                    successDrop1 = true;
                    successDrop3 = true;
                    counter += 10;
                    score += 10;
                    yFromTop = width / 5;
                    test = true;
                    test1 = true;
                    if (ismusic)
                        dropInBucketMusic.start();
                }

                if (test && score % 6 == 0) {
                    Log.d("Checking", "Score = " + score + " test= " + test);
                    lifeEndChecker = true;
                    test = false;
                }

                //////////////////////////////////////////////////////////////////////////change
                if (test1 && score > 0 && score % 100 == 0 && ySpeed < 15) {
                    ySpeed += 1;
                    ySpeed2 += .5;
                    test1 = false;
                }

                // POWER BOOSTER Concept

                if (powerDrop) {
                    powerCase = randomPos1.nextInt(4);    // Drop Position of powers.......
                    powerDrop = false;
                }

                if (ypowerTop < (height - bmpBucket.getHeight() / 2) && lifeEndChecker) {
                    switch (powerCase) {
                        case 0:
                            checking();
                            canvas.drawBitmap(bmpLifeUp, dropPosition2, ypowerTop, null);
                            bmp = bmpLifeUp;
                            break;
                        case 1:
                            checking();
                            canvas.drawBitmap(bmpLifeDown, dropPosition2, ypowerTop, null);
                            bmp = bmpLifeDown;
                            break;
                        case 2:
                            checking();
                            canvas.drawBitmap(bmpSpeedUp, dropPosition2, ypowerTop, null);
                            bmp = bmpSpeedUp;
                            break;
                        case 3:
                            checking();
                            canvas.drawBitmap(bmpScoreMinus, dropPosition2, ypowerTop, null);
                            bmp = bmpScoreMinus;
                            break;

                    }
                    ypowerTop += ySpeed2;
                } else {
                    ypowerTop = width / 5;
                    powerDrop = true;
                    lifeEndChecker = false;
                    successDrop2 = true;

                }

                // Checking For Life Up

                if (((x - (bmpBucket.getWidth() / 2) < dropPosition2 + (bmpLifeUp.getWidth() / 2)) &&
                        ((ypowerTop + (bmpLifeUp.getHeight() / 3)) >= height - (bmpBucket.getHeight() + 10)) &&
                        ((x + bmpBucket.getWidth() / 2) > dropPosition2 + (bmpLifeUp.getWidth() / 2)) &&
                        (ypowerTop + (bmpLifeUp.getHeight() / 3)) <= height - 10) &&
                        bmp == bmpLifeUp) {
                    if (ismusic)
                        dropInBucketMusic.start();
                    if (lifeCounter < 3)
                        lifeCounter++;
                    ypowerTop = width / 5;
                    lifeEndChecker = false;
                    successDrop2 = true;
                } else if (((x - (bmpBucket.getWidth() / 2) < dropPosition2 + (bmpLifeDown.getWidth() / 2)) &&
                        ((ypowerTop + (bmpLifeDown.getHeight() / 3)) >= height - (bmpBucket.getHeight() + 10)) &&
                        ((x + bmpBucket.getWidth() / 2) > dropPosition2 + (bmpLifeDown.getWidth() / 2)) &&
                        (ypowerTop + (bmpLifeDown.getHeight() / 3)) <= height - 10) &&
                        bmp == bmpLifeDown) {
                    if (ismusic)
                        dropInBucketMusic.start();
                    if (lifeCounter > 0)
                        lifeCounter--;
                    ypowerTop = width / 5;
                    lifeEndChecker = false;
                    successDrop2 = true;
                } else if (((x - (bmpBucket.getWidth() / 2) < dropPosition2 + (bmpSpeedUp.getWidth() / 2)) &&
                        ((ypowerTop + (bmpSpeedUp.getHeight() / 3)) >= height - (bmpBucket.getHeight() + 10)) &&
                        ((x + bmpBucket.getWidth() / 2) > dropPosition2 + (bmpSpeedUp.getWidth() / 2)) &&
                        (ypowerTop + (bmpSpeedUp.getHeight() / 3)) <= height - 10) &&
                        bmp == bmpSpeedUp) {
                    if (ismusic)
                        dropInBucketMusic.start();
                    ySpeed = ySpeed + 1;
                    ypowerTop = width / 5;
                    lifeEndChecker = false;
                    successDrop2 = true;
                } else if (((x - (bmpBucket.getWidth() / 2) < dropPosition2 + (bmpScoreMinus.getWidth() / 2)) &&
                        ((ypowerTop + (bmpScoreMinus.getHeight() / 3)) >= height - (bmpBucket.getHeight() + 10)) &&
                        ((x + bmpBucket.getWidth() / 2) > dropPosition2 + (bmpScoreMinus.getWidth() / 2)) &&
                        (ypowerTop + (bmpScoreMinus.getHeight() / 3)) <= height - 10) &&
                        bmp == bmpScoreMinus) {
                    if (ismusic)
                        dropInBucketMusic.start();
                    if (score > 0)
                        score = score - 10;
                    ypowerTop = width / 5;
                    lifeEndChecker = false;
                    successDrop2 = true;
                }

                // Creating bundle of score

                String strCheck = String.valueOf(score);
                Bundle gameScore = new Bundle();
                gameScore.putString("PlayerScore", strCheck);

                //Checking status of life of Game
				/*if(lifeCounter==4)
						canvas.drawBitmap(bmpFour, width-bmpThree.getWidth()-3, height-bmpThree.getHeight()-8, null);								   
					if(lifeCounter==3)
						canvas.drawBitmap(bmpThree, width-bmpThree.getWidth()-3, height-bmpThree.getHeight()-8, null);
					if(lifeCounter==2)
						canvas.drawBitmap(bmpTwo, width-bmpThree.getWidth()-3, height-bmpThree.getHeight()-8, null);
					if(lifeCounter==1)
						canvas.drawBitmap(bmpOne, width-bmpThree.getWidth()-3, height-bmpThree.getHeight()-8, null);
					if(lifeCounter==0)*/


                if (lifeCounter == 4)
                    canvas.drawBitmap(bmpFour, width - bmpThree.getWidth() - 3, bmpStone.getHeight() / 5, null);
                if (lifeCounter == 3)
                    canvas.drawBitmap(bmpThree, width - bmpThree.getWidth() - 3, bmpStone.getHeight() / 5, null);
                if (lifeCounter == 2)
                    canvas.drawBitmap(bmpTwo, width - bmpThree.getWidth() - 3, bmpStone.getHeight() / 5, null);
                if (lifeCounter == 1)
                    canvas.drawBitmap(bmpOne, width - bmpThree.getWidth() - 3, bmpStone.getHeight() / 5, null);
                if (lifeCounter == 0) {
                    Log.d(TAG, "run() called with: " + lifeCounter);
                    canvas.drawBitmap(bmpZero, width - bmpThree.getWidth() - 3, bmpStone.getHeight() / 5, null);

                    lifeCounter = 1;


//					FruitBucketEasyMode.this.finish();
                    paused = false;
					Intent intent=new Intent(FruitBucketEasyMode.this,SaveMe.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtras(gameScore);
					intent.putExtra("id", 1);                                        // TESTING
					startActivity(intent);

                }

                //Applying  Touch Concept on Bucket

                if (x == 0 && y == 0) {
                    //				canvas.drawBitmap(bmpBucket,canvas.getWidth()/2-bmpBucket.getWidth()/2,height-2*(bmpBucket.getHeight()), null);
                    canvas.drawBitmap(bmpBucket, width / 2 - bmpBucket.getWidth() / 2, height - (bmpBucket.getHeight() + 10), null);
                }

                if (x != 0 && y != 0) {
                    if (counter >= 9000) {
                        // Checking Both Left and Right Collision of Bucket With walls

                        if (x <= bmpBucket.getWidth() / 2 || x >= width - bmpBucket.getWidth() / 2) {

                            if (x <= bmpBucket.getWidth() / 2)        // Left Wall collision
                            {
                                x = bmpBucket.getWidth() / 2;
                            }

                            if (x >= width - bmpBucket.getWidth() / 2) // Right Wall Collision
                            {
                                x = width - bmpBucket.getWidth() / 2;
                            }

                            //						canvas.drawBitmap(bmpBucket,x-(bmpBucket.getWidth()/2),height-2*(bmpBucket.getHeight()),null);
                            canvas.drawBitmap(bmpBucket, x - (bmpBucket.getWidth() / 2), height - (bmpBucket.getHeight() + 10), null);
                        } else {
                            //						canvas.drawBitmap(bmpBucket,x-(bmpBucket.getWidth()/2),height-2*(bmpBucket.getHeight()),null);
                            canvas.drawBitmap(bmpBucket, x - (bmpBucket.getWidth() / 2), height - (bmpBucket.getHeight() + 10), null);
                        }
                    } else {
                        if (x <= bmpBucket.getWidth() / 2 || x >= width - bmpBucket.getWidth() / 2) {
                            if (x <= bmpBucket.getWidth() / 2) {
                                x = bmpBucket.getWidth() / 2; //
                            }

                            if (x >= width - bmpBucket.getWidth() / 2) {
                                x = width - bmpBucket.getWidth() / 2;
                            }

                            //						canvas.drawBitmap(bmpBucket,x-(bmpBucket.getWidth()/2),height-2*(bmpBucket.getHeight()),null);
                            canvas.drawBitmap(bmpBucket, x - (bmpBucket.getWidth() / 2), height - (bmpBucket.getHeight() + 10), null);
                        } else {
                            //						canvas.drawBitmap(bmpBucket,x-(bmpBucket.getWidth()/2),height-2*(bmpBucket.getHeight()),null);
                            canvas.drawBitmap(bmpBucket, x - (bmpBucket.getWidth() / 2), height - (bmpBucket.getHeight() + 10), null);
                        }

                    }
                }
                canvas.drawText(scoreUpdate(), bmpSCard.getWidth() + 5, bmpSCard.getHeight(), paintmyscore);

                holder.unlockCanvasAndPost(canvas);
            }
        }

    }
}