package alienapps.com.perkgame.savefruit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.perk.perksdk.PerkListener;
import com.perk.perksdk.PerkManager;

import alienapps.com.perkgame.R;

/**
 * Created by anil on 20/12/15.
 */

public class SaveMe extends Activity {

    String key = "83a52f4d296807b2918eac10ad2eb389a1d035f3";
    boolean isEventTrrigered = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_saave_me);

        final PerkListener localPerkListener = new PerkListener() {
            @Override
            public void onPerkEvent(String reason) {
                Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_SHORT).show();
                if (reason.equalsIgnoreCase("SDK Failed")) {
                    isEventTrrigered = true;
                    giveOneChance();
                }
            }
        };

        PerkManager.startSession(SaveMe.this, key, localPerkListener);
    }

    public void prsMethod(View v)
    {
        Intent intent=getIntent();
        switch(v.getId())
        {
            case R.id.cancle: {
                        gameOver(intent);
            }

                break;
            case R.id.continu:
                Log.d("", "Hi donot divert");
                PerkManager.trackEvent(this, key, "f1b3f72be6a3fb4dbe2c2a5f1c2d584a88e07cf2", true, null);

                break;

        }
    }

    private void giveOneChance() {
        Intent intent = getIntent();
        if (isEventTrrigered) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            SaveMe.this.finish();
            isEventTrrigered = false;
        }
    }

    private void gameOver(Intent intent) {
        PerkManager.showAdUnit("df134859d8e6f27e73cf4a3d8e534cc2e647c86b");
        int id = intent.getIntExtra("id", 1);
        Bundle bundle = intent.getExtras();
        SaveMe.this.finish();
        Intent intent1 = new Intent(SaveMe.this, GameOver.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent1.putExtras(bundle);
        intent1.putExtra("id", id);
        startActivity(intent1);
    }

    @Override
    protected void onRestoreInstanceState (Bundle restoreBundle)
    {
        super.onRestoreInstanceState(restoreBundle);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=getIntent();
        gameOver(intent);
    }
}
